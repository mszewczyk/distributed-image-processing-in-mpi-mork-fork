#ifndef SETTINGS_H
#define SETTINGS_H
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
enum class prefilterType
{
    none=0,
    statistic=1,
    median=2
};
enum class postfilterType
{
    none=0,
    impulseFilter=1,
    impulseFilterWithMedian=2,
    impulseFilterBilinear=4,
    medianFilter=3
};

enum class colorMatrixType
{
    none=0,
    dcrawDefault=1
};
enum class demosaicMethod
{
    nearestNeighbor=0,
    bilinear=1,
    simplegradients=2,
    VNG=3
};
enum class outputFormat{
	png=0

};
/** Partial result data, a rectangle starting at point (startingHeight, startingWidth),
 * with dimensions realHeight and realWidth
 */
class OutputData{
public:
	//virtual ~OutputData();
	//OutputData(const OutputData &outputData);
	std::vector<char> buffer;
	uint precision = 8;
	uint startingHeight=0;
	uint startingWidth=0;
	uint realHeight=0;
	uint realWidth=0;
    ///boost archive
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & precision;
        ar & startingHeight;
        ar & startingWidth;
        ar & realHeight;
        ar & realWidth;
        ar & buffer;
    }
};

class InputData
{

public:
    InputData();
    const char * getDemosaicName()
    {
        int t = static_cast<int>(demosaic);
        if(t<demosaicMethodsSize)
            return demosaicMethods[t];
        else
            return "";
    }
    virtual ~InputData();
    //data defaults
    int precision=8;
    unsigned int blackLevel=0;
    bool customBlackLevel=false;
    prefilterType prefilter=prefilterType::none;
    unsigned int prefilterMask=5;
    postfilterType postfilter=postfilterType::none;
    unsigned int postfilterMask=7;
    unsigned int saturationLevel=0;
    bool customSaturationLevel=false;
    colorMatrixType colorMatrix=colorMatrixType::dcrawDefault;
    demosaicMethod demosaic=demosaicMethod::VNG;
    outputFormat format=outputFormat::png;
    bool verbose=true;
    std::vector<char> buffer;
    ///boost archive
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & blackLevel;
        ar & customBlackLevel;
        ar & prefilter;
        ar & postfilter;
        ar & saturationLevel;
        ar & customSaturationLevel;
        ar & colorMatrix;
        ar & demosaic;
        ar & precision;
        ar & buffer;
    }
protected:


private:
    static const int demosaicMethodsSize=4;
    const char* demosaicMethods[demosaicMethodsSize] =
    {
        "nearestNeighbor","bilinear","simplegradients", "VNG"
    };

};

#endif // SETTINGS_H
